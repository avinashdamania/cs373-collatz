#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    round_up_to_nearest_thousand,
    round_down_to_nearest_thousand,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # round_up_to_nearest_thousand
    # ----
    def test_round_up_1(self):
        n = 1
        rounded = round_up_to_nearest_thousand(n)
        self.assertEqual(rounded, 1000)

    def test_round_up_2(self):
        n = 30000
        rounded = round_up_to_nearest_thousand(n)
        self.assertEqual(rounded, 30000)

    def test_round_up_3(self):
        n = 1365
        rounded = round_up_to_nearest_thousand(n)
        self.assertEqual(rounded, 2000)

    # ----
    # round_down_to_nearest_thousand
    # ----
    def test_round_down_1(self):
        n = 13756
        rounded = round_down_to_nearest_thousand(n)
        self.assertEqual(rounded, 13000)

    def test_round_down_2(self):
        n = 1001
        rounded = round_down_to_nearest_thousand(n)
        self.assertEqual(rounded, 1000)

    def test_round_down_3(self):
        n = 2000
        rounded = round_up_to_nearest_thousand(n)
        self.assertEqual(rounded, 2000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(23453, 15968)
        self.assertEqual(v, 279)

    def test_eval_6(self):
        v = collatz_eval(631986, 968076)
        self.assertEqual(v, 525)

    def test_eval_7(self):
        v = collatz_eval(114062, 36098)
        self.assertEqual(v, 354)

    def test_eval_8(self):
        v = collatz_eval(490885, 945991)
        self.assertEqual(v, 525)

    def test_eval_9(self):
        v = collatz_eval(781752, 487719)
        self.assertEqual(v, 509)

    def test_eval_10(self):
        v = collatz_eval(91477, 153603)
        self.assertEqual(v, 375)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
